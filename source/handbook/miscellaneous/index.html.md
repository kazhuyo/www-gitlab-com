---
layout: markdown_page
title: "Miscellaneous"
---

### Secret Santa 2016

This year a Secret Santa will be organized. This is an event where participants exchange gifts with another participant. Participants only know to whom they have to send a gift. Because this event takes place around Christmas, and as each participant has no clue who will send them a gift this is called 'Secret Santa'.

Rules/guidelines:
- Wait for the email to tell you who you'll have to ship a present to
- Get that person a gift and ship this gift to him/her
  - This should be done so it arrives before christmas
    - Or you can ship it on the 22nd of December :shipit:
- The budget is € 20 for the gift (excluding shipping), paid out of pocket.
- You'll post a picture of your gift in #random, to thank your secret Santa!
- Perfectly content, you wait until next year to participate again... :)

##### F.A.Q.

> But I have this super duper awesome gift idea and its over the budget

Well, this really depends on the situation and the fact that you actually have this question means you're probably not over budget by €2. ;)

> I found this amazing, best gift ever and its under budget?

That is perfectly fine. It is more important to give something awesome than to spend € 20.

> Wait, potentially I have to ship something to the other side of the world!

You don't have to, there is an Amazon, or something like it in most countries. Let them do the heavy lifting! If you can't come up with a solution, please ask @zj on Slack.

> Shipping my trebuchet will be expensive, can I take it with me to Mexico (summit) and give it there?

For a full size trebuchet I'll make an exception, other than that, please do not bring your gift to Mexico. This means a couple of things:
1. The other participant has to have some spare room in their suitcase
1. Some people will already have a gift, I wouldn't be able to handle the suspense!
1. No clue, just ship it!

> Can I add a poem to the package?

So, if you're referring to the good old Sinterklaasgedichten, yes! You can and should! Just keep in mind not everyone is familiar with tone usually used in them.

> Cool idea, but I'd rather not be a participant this year.

That is ok! No hard feelings, and maybe next year!
